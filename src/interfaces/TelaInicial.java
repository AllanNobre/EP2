package interfaces;

import static classes.Imagem.VerificaImagem;
import classes.PGM;
import classes.PPM;
import java.io.File;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class TelaInicial extends javax.swing.JFrame {
    //Construtor
    public TelaInicial() {
        initComponents();
        setVisible(true);
    }

    //Código Gerado
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblExercProgr = new javax.swing.JLabel();
        btnAbrirImagem = new javax.swing.JButton();
        LogoJava = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Orientação a Objetos - by: Allan Jefrey");
        setPreferredSize(new java.awt.Dimension(665, 275));
        setResizable(false);
        setSize(new java.awt.Dimension(665, 275));

        lblExercProgr.setFont(new java.awt.Font("Ubuntu", 3, 22)); // NOI18N
        lblExercProgr.setForeground(new java.awt.Color(22, 90, 246));
        lblExercProgr.setText("  Exercício de Programação 2 - Java ");
        lblExercProgr.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(231, 56, 52), 3, true));

        btnAbrirImagem.setFont(new java.awt.Font("Ubuntu", 1, 20)); // NOI18N
        btnAbrirImagem.setForeground(new java.awt.Color(22, 90, 246));
        btnAbrirImagem.setText("Abrir Imagem");
        btnAbrirImagem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAbrirImagemActionPerformed(evt);
            }
        });

        LogoJava.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaces/imagens/Java-icon.png"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(LogoJava)
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblExercProgr, javax.swing.GroupLayout.PREFERRED_SIZE, 372, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(92, 92, 92)
                        .addComponent(btnAbrirImagem, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(LogoJava))
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(lblExercProgr, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(62, 62, 62)
                .addComponent(btnAbrirImagem, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnAbrirImagemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbrirImagemActionPerformed
        JFileChooser escolha = new JFileChooser();
        
        // Aplicando exceção de arquivos
        escolha.setFileFilter(new FileNameExtensionFilter("Imagem ppm ou pgm", "ppm", "pgm"));
        escolha.setAcceptAllFileFilterUsed(false);
        escolha.showOpenDialog(this);
        
        //Abrindo Interface Correspondente a Imagem e Verificando Tipo
        try {
            File arquivo = escolha.getSelectedFile();
            // Se for .pgm
            if (VerificaImagem(arquivo.getPath()) == 1) {
                PGM pgm = new PGM(arquivo.getPath());
                TelaPgm interfacePgm = new TelaPgm(pgm);
                interfacePgm.setVisible(true);
                dispose();
            // Se for .ppm
            }else if(VerificaImagem(arquivo.getPath()) == 2) {
                PPM ppm = new PPM(arquivo.getPath());
                TelaPpm interfacePpm = new TelaPpm(ppm);
                interfacePpm.setVisible(true);
                dispose();
            } else {
                JOptionPane.showMessageDialog(null, "\nA imagem não é do tipo requerido!\nEscolha um diretório válido!", "Ops! Algo não está certo!", JOptionPane.ERROR_MESSAGE);
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage() + "\nEscolha um caminho válido!", "Ops! Algo não está certo!", JOptionPane.ERROR_MESSAGE);
        // Tratar exception estranha do botão cancelar
        }catch( NullPointerException npE ) {}
    }//GEN-LAST:event_btnAbrirImagemActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LogoJava;
    private javax.swing.JButton btnAbrirImagem;
    private javax.swing.JLabel lblExercProgr;
    // End of variables declaration//GEN-END:variables
}