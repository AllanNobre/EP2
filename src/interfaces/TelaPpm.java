package interfaces;

import static classes.Imagem.VerificaImagem;
import classes.PGM;
import classes.PPM;
import java.awt.Image;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class TelaPpm extends javax.swing.JFrame {
    //Atributos
    private PPM ppm;
    
    //Setters
    public void setPpm(PPM ppm) {    
        this.ppm = ppm;
    }
    
    //Getters
    public PPM getPpm() {
        return ppm;
    }
    
    //Construtor
    public TelaPpm(PPM ppm) {
        initComponents();
        setPpm(ppm);
        
        try {        
            //Chamando métodos da Classe Pgm
            ppm.abrirImagem();
            ppm.extrairDados();
            ppm.calculaTamanhoImagem();
        
            //Enviando Endereço e Número Mágico para os TextsFields adequados
            txtMostraEndereco.setText(ppm.getEndereco());
            txtMostraNumMagico.setText(ppm.getNumeroMagico());
            
            //Eviando imagem original para a Label de Imagem
            lblMostraImagem.setIcon(new ImageIcon(ppm.extrairPixels().getScaledInstance(600, 450, Image.SCALE_REPLICATE)));
        
            //Trata a execeção e volta para a tela inicial
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Não foi utiizar essa Imagem\nVolte e escolha um diretório válido!", "Ops! Algo não está certo!", JOptionPane.ERROR_MESSAGE);
            TelaInicial interfaceInicial = new TelaInicial();
            interfaceInicial.setVisible(true);
            dispose();
        }
    }

    //Código Gerado
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtMostraEndereco = new javax.swing.JTextField();
        lblTitleNumMagico = new javax.swing.JLabel();
        lblTitleEndereco1 = new javax.swing.JLabel();
        txtMostraNumMagico = new javax.swing.JTextField();
        cbxFiltros = new javax.swing.JComboBox<>();
        btnAplicarImagem = new javax.swing.JButton();
        btnSalvar = new javax.swing.JButton();
        btnEscolherOutraImagem = new javax.swing.JButton();
        lblMostraImagem = new javax.swing.JLabel();
        lblMostraImagemBorda = new javax.swing.JLabel();
        btnVoltarTelaInicial = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Imagens do Tipo .ppm");
        setPreferredSize(new java.awt.Dimension(1000, 550));
        setResizable(false);
        setSize(new java.awt.Dimension(1000, 550));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtMostraEndereco.setEditable(false);
        txtMostraEndereco.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        getContentPane().add(txtMostraEndereco, new org.netbeans.lib.awtextra.AbsoluteConstraints(124, 12, 560, 35));

        lblTitleNumMagico.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        lblTitleNumMagico.setText("Número Mágico:");
        getContentPane().add(lblTitleNumMagico, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 10, 150, 35));

        lblTitleEndereco1.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        lblTitleEndereco1.setText("Endereço:");
        getContentPane().add(lblTitleEndereco1, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 12, 100, 35));

        txtMostraNumMagico.setEditable(false);
        txtMostraNumMagico.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        getContentPane().add(txtMostraNumMagico, new org.netbeans.lib.awtextra.AbsoluteConstraints(882, 12, 60, 35));

        cbxFiltros.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        cbxFiltros.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Imagem Original", "Filtro Negativo", "Filtro Vermelho", "Filtro Verde", "Filtro Azul" }));
        getContentPane().add(cbxFiltros, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 110, 186, -1));

        btnAplicarImagem.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        btnAplicarImagem.setText("Aplicar");
        btnAplicarImagem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAplicarImagemActionPerformed(evt);
            }
        });
        getContentPane().add(btnAplicarImagem, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 160, 155, 54));

        btnSalvar.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        btnSalvar.setText("Salvar");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });
        getContentPane().add(btnSalvar, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 330, 155, 54));

        btnEscolherOutraImagem.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        btnEscolherOutraImagem.setText("Escolher Outra Imagem");
        btnEscolherOutraImagem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEscolherOutraImagemActionPerformed(evt);
            }
        });
        getContentPane().add(btnEscolherOutraImagem, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 400, 250, 54));
        getContentPane().add(lblMostraImagem, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 110, 600, 400));

        lblMostraImagemBorda.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createMatteBorder(4, 4, 4, 4, new java.awt.Color(67, 53, 53)), "Imagem", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Ubuntu", 3, 14))); // NOI18N
        getContentPane().add(lblMostraImagemBorda, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 80, 700, 440));

        btnVoltarTelaInicial.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        btnVoltarTelaInicial.setText("Tela Inicial");
        btnVoltarTelaInicial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVoltarTelaInicialActionPerformed(evt);
            }
        });
        getContentPane().add(btnVoltarTelaInicial, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 470, 155, 54));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnEscolherOutraImagemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEscolherOutraImagemActionPerformed
        JFileChooser escolha = new JFileChooser();
        
        // Aplicando exceção de arquivos
        escolha.setFileFilter(new FileNameExtensionFilter("Imagem ppm ou pgm", "ppm", "pgm"));
        escolha.setAcceptAllFileFilterUsed(false);
        escolha.showOpenDialog(this);
        
        //Abrindo Interface Correspondente a Imagem e Verificando Tipo
        try {
            File arquivo = escolha.getSelectedFile();
            // Se for .pgm
            if (VerificaImagem(arquivo.getPath()) == 1) {
                PGM pgm = new PGM(arquivo.getPath());
                TelaPgm interfacePgm = new TelaPgm(pgm);
                interfacePgm.setVisible(true);
                dispose();
            // Se for .ppm
            }else if(VerificaImagem(arquivo.getPath()) == 2) {
                PPM ppmLocal = new PPM(arquivo.getPath());
                TelaPpm interfacePpm = new TelaPpm(ppmLocal);
                interfacePpm.setVisible(true);
                dispose();
            } else {
                JOptionPane.showMessageDialog(null, "\nA imagem não é do tipo requerido!\nEscolha um diretório válido!", "Ops! Algo não está certo!", JOptionPane.ERROR_MESSAGE);
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage() + "\nEscolha um caminho válido!", "Ops! Algo não está certo!", JOptionPane.ERROR_MESSAGE);
        // Tratar exception estranha do botão cancelar
        }catch( NullPointerException npE ) {}
    }//GEN-LAST:event_btnEscolherOutraImagemActionPerformed

    private void btnAplicarImagemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAplicarImagemActionPerformed
        //Eviando imagem original para a Label de Imagem
        if (cbxFiltros.getSelectedItem().equals("Imagem Original")){
            try {
                lblMostraImagem.setIcon(new ImageIcon(ppm.extrairPixels().getScaledInstance(600, 450, Image.SCALE_REPLICATE)));
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "Não foi utiizar essa Imagem\nVolte e escolha um diretório válido!", "Ops! Algo não está certo!", JOptionPane.ERROR_MESSAGE);
                TelaInicial interfaceInicial = new TelaInicial();
                interfaceInicial.setVisible(true);
                dispose();
            }
            
        //Eviando imagem com Filtro Negativo para a Label de Imagem
        } else if (cbxFiltros.getSelectedItem().equals("Filtro Negativo")){
           lblMostraImagem.setIcon(new ImageIcon(ppm.aplicaFiltroNegativo().getScaledInstance(600, 450, Image.SCALE_REPLICATE)));
        
        //Eviando imagem com Filtro Vermelho para a Label de Imagem   
        } else if (cbxFiltros.getSelectedItem().equals("Filtro Vermelho")){
            lblMostraImagem.setIcon(new ImageIcon(ppm.aplicarFiltroRed().getScaledInstance(600, 450, Image.SCALE_REPLICATE)));
        
        //Eviando imagem com Filtro Verde para a Label de Imagem    
        }else if (cbxFiltros.getSelectedItem().equals("Filtro Verde")){
            lblMostraImagem.setIcon(new ImageIcon(ppm.aplicarFiltroGreen().getScaledInstance(600, 450, Image.SCALE_REPLICATE)));
        
        //Eviando imagem com Filtro Azul para a Label de Imagem
        } else if (cbxFiltros.getSelectedItem().equals("Filtro Azul")){
            lblMostraImagem.setIcon(new ImageIcon(ppm.aplicarFiltroBlue().getScaledInstance(600, 450, Image.SCALE_REPLICATE)));
        }
    }//GEN-LAST:event_btnAplicarImagemActionPerformed

    private void btnVoltarTelaInicialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVoltarTelaInicialActionPerformed
        TelaInicial interfaceInicial = new TelaInicial();
        interfaceInicial.setVisible(true);
        dispose();
    }//GEN-LAST:event_btnVoltarTelaInicialActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        JFileChooser salva = new JFileChooser();
        salva.showSaveDialog(this);
        try{
        //Pegando endereço e nome da nova Imagem
        File arquivo = salva.getSelectedFile();
                
        //Verifica Extensão
        if (!arquivo.getAbsolutePath().endsWith(".ppm")) {
            arquivo = new File(arquivo.getAbsolutePath() + ".ppm");
        } else {
            arquivo = new File(arquivo.getAbsolutePath());
        }
        
        //Criando nova imagem
        FileOutputStream temp;

        temp = new FileOutputStream(arquivo);
        if (!arquivo.exists()) {
            arquivo.createNewFile();
        }
        temp.write(getPpm().getNumeroMagico().getBytes());
        temp.write('\n');
        temp.write(Integer.toString(getPpm().getLargura()).getBytes());
        temp.write(' ');
        temp.write(Integer.toString(getPpm().getComprimento()).getBytes());
        temp.write('\n');
        temp.write(Integer.toString(getPpm().getNivelDeCor()).getBytes());
        temp.write('\n');
        
        //Vendo na Cbx qual imagem está aparecendo
        // Salva Imagem Original
        if (cbxFiltros.getSelectedItem().equals("Imagem Original")) {
            byte[] tmp = new byte[3 * getPpm().getComprimento() * getPpm().getLargura()];
            int i = 0;
            while( i <  3*getPpm().getComprimento() * getPpm().getLargura()) {
                
                tmp[i] = getPpm().getPixels()[i+2];
                i++;
                
                tmp[i] = getPpm().getPixels()[i];
                i++;
                
                tmp[i] = getPpm().getPixels()[i-2];
                i++;
        }
            temp.write(tmp);
            
        // Salva Imagem Negativa
        } else if (cbxFiltros.getSelectedItem().equals("Filtro Negativo")){
            getPpm().aplicaFiltroNegativo();
            byte[] tmp = new byte[3 * getPpm().getComprimento() * getPpm().getLargura()];
            int i = 0;
            while( i <  3*getPpm().getComprimento() * getPpm().getLargura()) {
                
                tmp[i] = getPpm().getPixelFiltrado()[i+2];
                i++;
                
                tmp[i] = getPpm().getPixelFiltrado()[i];
                i++;
                
                tmp[i] = getPpm().getPixelFiltrado()[i-2];
                i++;
        }
            temp.write(tmp);
            
        // Salva Imagem Vermelha   
        } else if (cbxFiltros.getSelectedItem().equals("Filtro Vermelho")){
            getPpm().aplicarFiltroRed();
            byte[] tmp = new byte[3 * getPpm().getComprimento() * getPpm().getLargura()];
            int i = 0;
            while( i <  3*getPpm().getComprimento() * getPpm().getLargura()) {
                
                tmp[i] = getPpm().getPixelFiltrado()[i+2];
                i++;
                
                tmp[i] = getPpm().getPixelFiltrado()[i];
                i++;
                
                tmp[i] = getPpm().getPixelFiltrado()[i-2];
                i++;
        }
            temp.write(tmp);
            
        // Salva Imagem Verde
        }else if (cbxFiltros.getSelectedItem().equals("Filtro Verde")){
            getPpm().aplicarFiltroGreen();
            byte[] tmp = new byte[3 * getPpm().getComprimento() * getPpm().getLargura()];
            int i = 0;
            while( i <  3*getPpm().getComprimento() * getPpm().getLargura()) {
                
                tmp[i] = getPpm().getPixelFiltrado()[i+2];
                i++;
                
                tmp[i] = getPpm().getPixelFiltrado()[i];
                i++;
                
                tmp[i] = getPpm().getPixelFiltrado()[i-2];
                i++;
        }
            temp.write(tmp);
            
        // Salva Imagem Azul
        } else if (cbxFiltros.getSelectedItem().equals("Filtro Azul")){
            getPpm().aplicarFiltroBlue();
            byte[] tmp = new byte[3 * getPpm().getComprimento() * getPpm().getLargura()];
            int i = 0;
            while( i <  3*getPpm().getComprimento() * getPpm().getLargura()) {
                
                tmp[i] = getPpm().getPixelFiltrado()[i+2];
                i++;
                
                tmp[i] = getPpm().getPixelFiltrado()[i];
                i++;
                
                tmp[i] = getPpm().getPixelFiltrado()[i-2];
                i++;
        }
            temp.write(tmp);
            
        }
        
        //Fecha Imagem tmp 
            temp.close();
            
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage() + "\nEscolha um caminho válido!", "Ops! Algo não está certo!", JOptionPane.ERROR_MESSAGE);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage() + "\nEscolha um caminho válido!", "Ops! Algo não está certo!", JOptionPane.ERROR_MESSAGE);
        }catch( NullPointerException npE ) {}
    }//GEN-LAST:event_btnSalvarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAplicarImagem;
    private javax.swing.JButton btnEscolherOutraImagem;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JButton btnVoltarTelaInicial;
    private javax.swing.JComboBox<String> cbxFiltros;
    private javax.swing.JLabel lblMostraImagem;
    private javax.swing.JLabel lblMostraImagemBorda;
    private javax.swing.JLabel lblTitleEndereco1;
    private javax.swing.JLabel lblTitleNumMagico;
    private javax.swing.JTextField txtMostraEndereco;
    private javax.swing.JTextField txtMostraNumMagico;
    // End of variables declaration//GEN-END:variables
}
