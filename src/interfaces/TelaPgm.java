package interfaces;

import static classes.Imagem.VerificaImagem;
import classes.PGM;
import classes.PPM;
import java.awt.Image;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class TelaPgm extends javax.swing.JFrame {
    //Atributos
    private PGM pgm;
    
    //Setters
    public void setPpm(PGM pgm) {    
        this.pgm = pgm;
    }
    
    //Getters
    public PGM getPpm() {
        return pgm;
    }
    
    //Construtor
    public TelaPgm(PGM pgm) {
        initComponents();
        setPpm(pgm);
        
        //Escondendo a barra de decifrar imagem
        txtPanelMostraMensagem.setVisible(false);

        try {        
            //Chamando métodos da Classe Pgm
            pgm.abrirImagem();
            pgm.extrairDados();
            pgm.extrairPosicaoMensagem();
            pgm.calculaTamanhoImagem();
            
            //Enviando Endereço e Número Mágico para os TextsFields adequados
            txtMostraEndereco.setText(pgm.getEndereco());
            txtMostraNumMagico.setText(pgm.getNumeroMagico());
                    
            //Eviando imagem original para a Label de Imagem
            lblMostraImagem.setIcon(new ImageIcon(pgm.extrairPixels().getScaledInstance(600, 450, Image.SCALE_REPLICATE)));
        
            //Trata a execeção e volta para a tela inicial
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Não foi utiizar essa Imagem\nVolte e escolha um diretório válido!", "Ops! Algo não está certo!", JOptionPane.ERROR_MESSAGE);
            TelaInicial interfaceInicial = new TelaInicial();
            interfaceInicial.setVisible(true);
            dispose();
        }
    }

    //Código Gerado
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtMostraEndereco = new javax.swing.JTextField();
        lblTitleNumMagico = new javax.swing.JLabel();
        lblTitleEndereco = new javax.swing.JLabel();
        txtMostraNumMagico = new javax.swing.JTextField();
        lblMostraImagem = new javax.swing.JLabel();
        cbxFiltros = new javax.swing.JComboBox<>();
        btnAplicarImagem = new javax.swing.JButton();
        btnSalvar = new javax.swing.JButton();
        btnEscolherOutraImagem = new javax.swing.JButton();
        btnDecifraMensagem = new javax.swing.JButton();
        lblMostraImagemBorda = new javax.swing.JLabel();
        btnVoltarTelaInicial = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtPanelMostraMensagem = new javax.swing.JTextPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Imagens do Tipo .pgm");
        setPreferredSize(new java.awt.Dimension(1000, 640));
        setResizable(false);
        setSize(new java.awt.Dimension(1000, 640));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtMostraEndereco.setEditable(false);
        txtMostraEndereco.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        getContentPane().add(txtMostraEndereco, new org.netbeans.lib.awtextra.AbsoluteConstraints(124, 12, 521, 35));

        lblTitleNumMagico.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        lblTitleNumMagico.setText("Número Mágico:");
        getContentPane().add(lblTitleNumMagico, new org.netbeans.lib.awtextra.AbsoluteConstraints(705, 12, 165, 35));

        lblTitleEndereco.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        lblTitleEndereco.setText("Endereço:");
        getContentPane().add(lblTitleEndereco, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 12, 100, 35));

        txtMostraNumMagico.setEditable(false);
        txtMostraNumMagico.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        getContentPane().add(txtMostraNumMagico, new org.netbeans.lib.awtextra.AbsoluteConstraints(882, 12, 60, 35));
        getContentPane().add(lblMostraImagem, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 90, 600, 400));

        cbxFiltros.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        cbxFiltros.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Imagem Original", "Filtro Negativo", "Filtro Sharpen", "Filtro Smooth" }));
        getContentPane().add(cbxFiltros, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 90, 186, -1));

        btnAplicarImagem.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        btnAplicarImagem.setText("Aplicar");
        btnAplicarImagem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAplicarImagemActionPerformed(evt);
            }
        });
        getContentPane().add(btnAplicarImagem, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 140, 155, 54));

        btnSalvar.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        btnSalvar.setText("Salvar");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });
        getContentPane().add(btnSalvar, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 280, 155, 54));

        btnEscolherOutraImagem.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        btnEscolherOutraImagem.setText("Escolher Outra Imagem");
        btnEscolherOutraImagem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEscolherOutraImagemActionPerformed(evt);
            }
        });
        getContentPane().add(btnEscolherOutraImagem, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 350, 250, 54));

        btnDecifraMensagem.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        btnDecifraMensagem.setText("Decifrar Mensagem");
        btnDecifraMensagem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDecifraMensagemActionPerformed(evt);
            }
        });
        getContentPane().add(btnDecifraMensagem, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 520, 213, 76));

        lblMostraImagemBorda.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createMatteBorder(4, 4, 4, 4, new java.awt.Color(67, 53, 53)), "Imagem", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Ubuntu", 3, 14))); // NOI18N
        getContentPane().add(lblMostraImagemBorda, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 60, 700, 440));

        btnVoltarTelaInicial.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        btnVoltarTelaInicial.setText("Tela Inicial");
        btnVoltarTelaInicial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVoltarTelaInicialActionPerformed(evt);
            }
        });
        getContentPane().add(btnVoltarTelaInicial, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 420, 155, 54));

        txtPanelMostraMensagem.setEditable(false);
        txtPanelMostraMensagem.setFont(new java.awt.Font("Nimbus Roman No9 L", 0, 18)); // NOI18N
        jScrollPane1.setViewportView(txtPanelMostraMensagem);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 520, 760, 80));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnEscolherOutraImagemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEscolherOutraImagemActionPerformed
        JFileChooser escolha = new JFileChooser();
        
        // Aplicando exceção de arquivos
        escolha.setFileFilter(new FileNameExtensionFilter("Imagem ppm ou pgm", "ppm", "pgm"));
        escolha.setAcceptAllFileFilterUsed(false);
        escolha.showOpenDialog(this);
        
        //Abrindo Interface Correspondente a Imagem e Verificando Tipo
        try {
            File arquivo = escolha.getSelectedFile();
            // Se for .pgm
            if (VerificaImagem(arquivo.getPath()) == 1) {
                PGM pgmLocal = new PGM(arquivo.getPath());
                TelaPgm interfacePgm = new TelaPgm(pgmLocal);
                interfacePgm.setVisible(true);
                dispose();
            // Se for .ppm
            }else if(VerificaImagem(arquivo.getPath()) == 2) {
                PPM ppm = new PPM(arquivo.getPath());
                TelaPpm interfacePpm = new TelaPpm(ppm);
                interfacePpm.setVisible(true);
                dispose();
            } else {
                JOptionPane.showMessageDialog(null, "\nA imagem não é do tipo requerido!\nEscolha um diretório válido!", "Ops! Algo não está certo!", JOptionPane.ERROR_MESSAGE);
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage() + "\nEscolha um caminho válido!", "Ops! Algo não está certo!", JOptionPane.ERROR_MESSAGE);
        // Tratar exception estranha do botão cancelar
        }catch( NullPointerException npE ) {}
    }//GEN-LAST:event_btnEscolherOutraImagemActionPerformed

    private void btnVoltarTelaInicialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVoltarTelaInicialActionPerformed
        TelaInicial interfaceInicial = new TelaInicial();
        interfaceInicial.setVisible(true);
        dispose();
    }//GEN-LAST:event_btnVoltarTelaInicialActionPerformed

    private void btnAplicarImagemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAplicarImagemActionPerformed
        //Eviando imagem original para a Label de Imagem
        if (cbxFiltros.getSelectedItem().equals("Imagem Original")){
            try {
                lblMostraImagem.setIcon(new ImageIcon(pgm.extrairPixels().getScaledInstance(600, 450, Image.SCALE_REPLICATE)));
            
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "Não foi utiizar essa Imagem\nVolte e escolha um diretório válido!", "Ops! Algo não está certo!", JOptionPane.ERROR_MESSAGE);
                TelaInicial interfaceInicial = new TelaInicial();
                interfaceInicial.setVisible(true);
                dispose();
            }
            
        //Eviando imagem com Filtro Negativo para a Label de Imagem
        } else if (cbxFiltros.getSelectedItem().equals("Filtro Negativo")){
           lblMostraImagem.setIcon(new ImageIcon(pgm.aplicaFiltroNegativo().getScaledInstance(600, 450, Image.SCALE_REPLICATE)));
        
        //Eviando imagem com Filtro Sharpen para a Label de Imagem   
        } else if (cbxFiltros.getSelectedItem().equals("Filtro Sharpen")){
            try {
                lblMostraImagem.setIcon(new ImageIcon(pgm.aplicarFiltroSharpen().getScaledInstance(600, 450, Image.SCALE_REPLICATE)));
  
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "Não foi utiizar essa Imagem\nVolte e escolha um diretório válido!", "Ops! Algo não está certo!", JOptionPane.ERROR_MESSAGE);
                TelaInicial interfaceInicial = new TelaInicial();
                interfaceInicial.setVisible(true);
                dispose();
            }
        
        //Eviando imagem com Filtro Smooth para a Label de Imagem
        }else if (cbxFiltros.getSelectedItem().equals("Filtro Smooth")){
            try {
                lblMostraImagem.setIcon(new ImageIcon(pgm.aplicarFiltroSmooth().getScaledInstance(600, 450, Image.SCALE_REPLICATE)));
            
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "Não foi utiizar essa Imagem\nVolte e escolha um diretório válido!", "Ops! Algo não está certo!", JOptionPane.ERROR_MESSAGE);
                TelaInicial interfaceInicial = new TelaInicial();
                interfaceInicial.setVisible(true);
                dispose();
            }
        }
    }//GEN-LAST:event_btnAplicarImagemActionPerformed

    private void btnDecifraMensagemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDecifraMensagemActionPerformed
        txtPanelMostraMensagem.setText(pgm.decifraImagem());
        txtPanelMostraMensagem.setVisible(true);
    }//GEN-LAST:event_btnDecifraMensagemActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        JFileChooser salva = new JFileChooser();
        salva.showSaveDialog(this);
        try{
        //Pegando endereço e nome da nova Imagem
        File arquivo = salva.getSelectedFile();
                
        //Verifica Extensão
        if (!arquivo.getAbsolutePath().endsWith(".pgm")) {
            arquivo = new File(arquivo.getAbsolutePath() + ".pgm");
        } else {
            arquivo = new File(arquivo.getAbsolutePath());
        }
        
        //Criando nova imagem
        FileOutputStream temp;

        temp = new FileOutputStream(arquivo);
        if (!arquivo.exists()) {
            arquivo.createNewFile();
        }
        temp.write(getPpm().getNumeroMagico().getBytes());
        temp.write('\n');
        temp.write(Integer.toString(getPpm().getLargura()).getBytes());
        temp.write(' ');
        temp.write(Integer.toString(getPpm().getComprimento()).getBytes());
        temp.write('\n');
        temp.write(Integer.toString(getPpm().getNivelDeCor()).getBytes());
        temp.write('\n');
        
        //Vendo na Cbx qual imagem está aparecendo
        // Salva Imagem Original
        if (cbxFiltros.getSelectedItem().equals("Imagem Original")) {
            temp.write(getPpm().getPixels());
            
        // Salva Imagem Negativa
        } else if (cbxFiltros.getSelectedItem().equals("Filtro Negativo")){
            getPpm().aplicaFiltroNegativo();
            temp.write(getPpm().getPixelFiltrado());
            
        // Salva Imagem Sharpen   
        } else if (cbxFiltros.getSelectedItem().equals("Filtro Sharpen")){
            getPpm().aplicarFiltroSharpen();
            temp.write(getPpm().getPixelFiltrado());
            
        // Salva Imagem Smooth
        }else if (cbxFiltros.getSelectedItem().equals("Filtro Smooth")){
            getPpm().aplicarFiltroSmooth();
            temp.write(getPpm().getPixelFiltrado());
        }
        
        //Fecha Imagem tmp 
            temp.close();
            
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage() + "\nEscolha um caminho válido!", "Ops! Algo não está certo!", JOptionPane.ERROR_MESSAGE);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage() + "\nEscolha um caminho válido!", "Ops! Algo não está certo!", JOptionPane.ERROR_MESSAGE);
        }catch( NullPointerException npE ) {}
    }//GEN-LAST:event_btnSalvarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAplicarImagem;
    private javax.swing.JButton btnDecifraMensagem;
    private javax.swing.JButton btnEscolherOutraImagem;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JButton btnVoltarTelaInicial;
    private javax.swing.JComboBox<String> cbxFiltros;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblMostraImagem;
    private javax.swing.JLabel lblMostraImagemBorda;
    private javax.swing.JLabel lblTitleEndereco;
    private javax.swing.JLabel lblTitleNumMagico;
    private javax.swing.JTextField txtMostraEndereco;
    private javax.swing.JTextField txtMostraNumMagico;
    private javax.swing.JTextPane txtPanelMostraMensagem;
    // End of variables declaration//GEN-END:variables
}
