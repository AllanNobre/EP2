package classes;

import java.awt.image.*;
import java.io.*;
import java.util.logging.*;
import javax.swing.JOptionPane;

public class PGM extends Imagem{
    //Atributos
    private int posicaoMensagem;
    
    //Consrutor
    public PGM(String endereco){
        posicaoMensagem = 0;
        setArquivo(null);
        setPixels(null);
        setNumeroMagico("");
        setEndereco(endereco);
        setLargura(0);
        setComprimento(0);
        setNivelDeCor(0);
        setTamanhoImagem(0);
        setPixelFiltrado(null);
    }
    
    //Setters
    public void setPosicaoMensagem(int posicaoMensagem) {
        this.posicaoMensagem = posicaoMensagem;
    }
    
    //Getters
    public int getPosicaoMensagem() {
        return posicaoMensagem;
    }

    //Outros Métodos
    public void extrairPosicaoMensagem() {
        try{
            String posicaoMensagemLocal = "";
            int contador = 0;
            int caracter;
            abrirImagem();
            
            //Pegando posição da mensagem que estará sempre no primeiro comentário da imagem PGM
            while (contador != 1){
                caracter = getArquivo().read();
                if ((char)caracter == '#'){
                    caracter = getArquivo().read();
                    do {
                        if (contador == 0){
                            caracter = getArquivo().read();
                            contador++;
                        }
                        posicaoMensagemLocal+=(char)caracter;
                        caracter = getArquivo().read();
                    } while((char)caracter != ' ' && (char)caracter != '\n');
                }
            }
            
            //Enviando para atributo
            setPosicaoMensagem(Integer.parseInt(posicaoMensagemLocal));
            
            //Fechando Imagem
            getArquivo().close();
            
        }catch (IOException ex){
            Logger.getLogger(Imagem.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex.getMessage() + "\nErro ao extrair a posição inicial da mensagem!", "Ops! Algo não está certo!", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public String decifraImagem() {
        String mensagemDecifrada = "";         // Armazena a mensagem decifrada  
        int contador_if = 0;             // Contador que determina a entrada no if
        int contador_ciclos = 0;        // Contador que define o termino de um ciclo de 8
        int encontra_sharp = 0;        // Para poder deixar o primeiro # da mensagem passar e parar no último
        char c = 0;
        char c1;

        for(int i = getPosicaoMensagem(); i < getTamanhoImagem(); i++) {
            contador_if++;
            if (contador_if == 1){
                c = (char) getPixels()[i];      // Binário qualquer -> xxxxxxxx
                c = (char)(c & 0x01);          // Binário menos significativo -> 0000000x
                c = (char) (c << 1);          // Binário menos significativo movido uma casa pra esquerda -> 000000x0 

                contador_ciclos++;
            } else if (contador_if == 2) {
                c1 = (char) getPixels()[i];     // Binário qualquer -> xxxxxxxx
                c1 = (char) (c1 & 0x01);       // Binário menos significativo -> 0000000x
                c = (char) (c | c1);          // Binários unidos -> 000000x0 + 0000000x = 000000xx

                contador_ciclos++;

                if (contador_ciclos < 8) {
                    c =(char) (c << 1);        // Binário movido pra esquerda -> 00000xx0
                    contador_if = 1;          // Para não entrar no primeiro if
                } else {
		//Esquema para printar a mensagem do início ao ultimo char antes do sharp
                    if (encontra_sharp == 0) {
                    // Printa o primeiro sharp	
                        encontra_sharp = 1;     // Evita a entrada nesse if novamente pois já foi printado o primeiro sharp

                        mensagemDecifrada += c;     // Guarda na String o char extraído
                        contador_if = 0;           // Zera os valores para poder reiniciar a extração
                        contador_ciclos = 0;      // Zera os valores para poder reiniciar a extração
                    } else {
                        if (c == '#') {
                            // Printa o último sharp e quebra o laço 'for' 		
                            mensagemDecifrada += "\n\nMensagem Decifrada, Fim!";     // Guarda na String o char extraído;  // Printa no terminal as quebras de linha, não printa o ultimo sharp
                            break;
                        } else {
                            // Se não for um sharp, continua a extração normalmente
                            mensagemDecifrada += c;     // Guarda na String o char extraído
                            contador_if = 0;           // Zera os valores para poder reiniciar a extração
                            contador_ciclos = 0;      // Zera os valores para poder reiniciar a extração
                        }
                    }
                }
            }
        }
        
        return mensagemDecifrada;
    }
    
    public BufferedImage aplicarFiltroSharpen() throws IOException {
        int contador = 0;     // Contador para mover o cursor
        int contador1 = 0;    // Contador para
        int caracter;
        abrirImagem();
            
        //Movendo cursor para caracter inicial da imagem
        while (contador != 3){
            contador++;
            caracter = getArquivo().read();
            if ((char)caracter == '#'){
                contador --;
                while((char)caracter != '\n') {
                    caracter = getArquivo().read();
                }
            } else {
                while((char)caracter != '\n') {
                    caracter = getArquivo().read();
                }
            }
        }
        caracter = getArquivo().read();
            
        //Extraindo os píxels da imagem em forma de char
        BufferedImage imagemSharpen = new BufferedImage(getLargura(), getComprimento(), BufferedImage.TYPE_BYTE_GRAY);
        @SuppressWarnings("MismatchedReadAndWriteOfArray")
        byte[] pixelsLocalByte = ((DataBufferByte) imagemSharpen.getRaster().getDataBuffer()).getData();
        @SuppressWarnings("MismatchedReadAndWriteOfArray")
        char[] pixelsLocalChar = new char [getTamanhoImagem()];
            
        while(caracter!=-1){
            pixelsLocalChar[contador1]+=(char)caracter;
            caracter=getArquivo().read();
            contador1++; // -> Indica qual o tamanho da imagem (de 1 a contador1)
        }
            
        //Fechando Imagem
        getArquivo().close();
            
        // Aplicando o filtro no Array de char
        int contador2 = 1;
        char[] tmp = new char [getTamanhoImagem()];
        for (int i = 0; i < getTamanhoImagem(); i++){
            if (i > (getComprimento()-1) && i < (getTamanhoImagem() - getComprimento())){
                pixelsLocalByte[i] = (byte) pixelsLocalChar[i];
                tmp[i] = pixelsLocalChar[i];
                i++;
                //Aplica filtro
                while (contador2 != (getComprimento()-1)){                   
                //Píxels Modificados:       Meio                       Direita                        Esquerda                              Cima                                       Baixo                                             Baixo/Direita                              Baixo/Esquerda                                    Cima/Direita                                 Cima/Esquerda             
                    tmp[i] = (char) ((pixelsLocalChar[i])*5 + (pixelsLocalChar[i + 1])*(-1) + (pixelsLocalChar[i - 1])*(-1) + (pixelsLocalChar[i - getComprimento()])*(-1) + (pixelsLocalChar[i + getComprimento()])*(-1) + (pixelsLocalChar[i + getComprimento() +1 ])*0 + (pixelsLocalChar[i + getComprimento() - 1])*0 + (pixelsLocalChar[i - getComprimento() + 1])*0 + (pixelsLocalChar[i - getComprimento() - 1])*0);
                    
                    if (tmp[i] > (char) getNivelDeCor()){
                        tmp[i] = (char) getNivelDeCor();
                    } else if (tmp[i] < (char)0){
                        tmp[i] = (char) 0;
                    }
                        
                    pixelsLocalByte[i] = (byte) tmp[i];
                    
                    contador2 ++;
                    i++;
                }
                contador2 = 1;
            } else {
                pixelsLocalByte[i] = (byte) pixelsLocalChar[i];
                tmp[i] = pixelsLocalChar[i];
            }
        } 
        
        //Envia para o atributo utilizado para poder salvar a imagem
        setPixelFiltrado(pixelsLocalByte);
        
        return imagemSharpen;
    }
    
    public BufferedImage aplicarFiltroSmooth() throws IOException {
        int contador = 0;     // Contador para mover o cursor
        int contador1 = 0;    // Contador para
        int caracter;
        abrirImagem();
            
        //Movendo cursor para caracter inicial da imagem
        while (contador != 3){
            contador++;
            caracter = getArquivo().read();
            if ((char)caracter == '#'){
                contador --;
                while((char)caracter != '\n') {
                    caracter = getArquivo().read();
                }
            } else {
                while((char)caracter != '\n') {
                    caracter = getArquivo().read();
                }
            }
        }
        caracter = getArquivo().read();
            
        //Extraindo os píxels da imagem em forma de char
        BufferedImage imagemSmooth = new BufferedImage(getLargura(), getComprimento(), BufferedImage.TYPE_BYTE_GRAY);
        @SuppressWarnings("MismatchedReadAndWriteOfArray")
        byte[] pixelsLocalByte = ((DataBufferByte) imagemSmooth.getRaster().getDataBuffer()).getData();
        @SuppressWarnings("MismatchedReadAndWriteOfArray")
        char[] pixelsLocalChar = new char [getTamanhoImagem()];
            
        while(caracter!=-1){
            pixelsLocalChar[contador1]+=(char)caracter;
            caracter=getArquivo().read();
            contador1++; // -> Indica qual o tamanho da imagem (de 1 a contador1)
        }
            
        //Fechando Imagem
        getArquivo().close();
            
        // Aplicando o filtro no Array de char
        int contador2 = 1;
        char[] tmp = new char [getTamanhoImagem()];
        for (int i = 0; i < getTamanhoImagem(); i++){
            if (i > (getComprimento()-1) && i < (getTamanhoImagem() - getComprimento())){
                pixelsLocalByte[i] = (byte) pixelsLocalChar[i];
                tmp[i] = pixelsLocalChar[i];
                i++;
                //Aplica filtro
                while (contador2 != (getComprimento()-1)){                   
                //Píxels Modificados:       Meio                     Direita                  Esquerda                        Cima                                  Baixo                                             Baixo/Direita                              Baixo/Esquerda                                    Cima/Direita                                 Cima/Esquerda             
                    tmp[i] = (char) ((pixelsLocalChar[i]*1 + pixelsLocalChar[i + 1]*1 + pixelsLocalChar[i - 1]*1 + pixelsLocalChar[i - getComprimento()]*1 + pixelsLocalChar[i + getComprimento()]*1 + pixelsLocalChar[i + getComprimento() +1 ]*1 + pixelsLocalChar[i + getComprimento() - 1]*1 + pixelsLocalChar[i - getComprimento() + 1]*1 + pixelsLocalChar[i - getComprimento() - 1]*1)/9);
                    
                    if (tmp[i] > (char) getNivelDeCor()){
                        tmp[i] = (char) getNivelDeCor();
                    } else if (tmp[i] < (char) 0){
                        tmp[i] = (char) 0;
                    }
                        
                    pixelsLocalByte[i] = (byte) tmp[i];
                    
                    contador2 ++;
                    i++;
                }
                contador2 = 1;
            } else {
                pixelsLocalByte[i] = (byte) pixelsLocalChar[i];
                tmp[i] = pixelsLocalChar[i];
            }
        }
        
        //Envia para o atributo utilizado para poder salvar a imagem
        setPixelFiltrado(pixelsLocalByte);
        
        return imagemSmooth;
    }
    
    @Override
    public BufferedImage extrairPixels() throws IOException{
        int contador = 0;
        int contador1 = 0;
        int caracter;
        abrirImagem();
            
        //Movendo cursor para caracter inicial da imagem
        while (contador != 3){
            contador++;
            caracter = getArquivo().read();
            if ((char)caracter == '#'){
                contador --;
                while((char)caracter != '\n') {
                    caracter = getArquivo().read();
                }
            } else {
                while((char)caracter != '\n') {
		caracter = getArquivo().read();
                }
            }
        }
        caracter = getArquivo().read();
            
        //Extraindo os píxels da imagem em forma de Bytes
        BufferedImage imagemPGM = new BufferedImage(getLargura(), getComprimento(), BufferedImage.TYPE_BYTE_GRAY);
        byte[] pixelsLocal = ((DataBufferByte) imagemPGM.getRaster().getDataBuffer()).getData();
            
        while(caracter!=-1){
            pixelsLocal[contador1] = (byte)caracter;
            caracter = getArquivo().read();
            contador1++; // -> Indica qual o tamanho da imagem (de 1 a contador1)
        }
            
        //Enviando para atributo
        setPixels(pixelsLocal);
            
        //Fechando Imagem
        getArquivo().close();
            
        return imagemPGM;
    }

    @Override
    public void calculaTamanhoImagem() {
        setTamanhoImagem(getLargura()*getComprimento());
    }

    @Override
    public BufferedImage aplicaFiltroNegativo() {
        BufferedImage imagemNegativa = new BufferedImage(getLargura(), getComprimento(), BufferedImage.TYPE_BYTE_GRAY);
        @SuppressWarnings("MismatchedReadAndWriteOfArray")
        byte[] pixelsLocal = ((DataBufferByte) imagemNegativa.getRaster().getDataBuffer()).getData();
        
        for (int i = 0; i < getTamanhoImagem(); i++) {
            pixelsLocal[i]=(byte) (getNivelDeCor() - getPixels()[i]);
        }
        
        //Envia para o atributo utilizado para poder salvar a imagem
        setPixelFiltrado(pixelsLocal);
        
        return imagemNegativa;
    }
}