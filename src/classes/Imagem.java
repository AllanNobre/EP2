package classes;

import java.awt.image.BufferedImage;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public abstract class Imagem {
    //Atributos
    private FileInputStream arquivo;
    private byte[] pixels;
    private String numeroMagico;
    private String endereco;
    private int largura;      // <- Número de Linhas |
    private int comprimento;  // <- Número de Colunas --
    private int nivelDeCor;
    private int tamanhoImagem;
    private byte[] pixelFiltrado;
    
    //Construtor
    public Imagem () {
        arquivo = null;
        pixels = null;
        numeroMagico = "";
        endereco = "";
        largura = 0;
        comprimento = 0;
        nivelDeCor = 0;
        tamanhoImagem = 0;
    }

    //Setters 
    public void setArquivo(FileInputStream arquivo) {
        this.arquivo = arquivo;
    }
    public void setPixels(byte[] pixels) {
        this.pixels = pixels;
    }
    public void setNumeroMagico(String numeroMagico) {
        this.numeroMagico = numeroMagico;
    }
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }
    public void setLargura(int largura) {
        this.largura = largura;
    }
    public void setComprimento(int comprimento) {
        this.comprimento = comprimento;
    }
    public void setNivelDeCor(int nivelDeCor) {
        this.nivelDeCor = nivelDeCor;
    }
    public void setTamanhoImagem(int tamanhoImagem) {
        this.tamanhoImagem = tamanhoImagem;
    }
    public void setPixelFiltrado(byte[] pixelFiltrado) {
        this.pixelFiltrado = pixelFiltrado;
    }
    
    //Getters
    public FileInputStream getArquivo() {
        return arquivo;
    }
    public byte[] getPixels() {
        return pixels;
    }
    public String getNumeroMagico() {
        return numeroMagico;
    }
    public String getEndereco() {
        return endereco;
    }
    public int getLargura() {
        return largura;
    }
    public int getComprimento() {
        return comprimento;
    }
    public int getNivelDeCor() {
        return nivelDeCor;
    }
    public int getTamanhoImagem() {
        return tamanhoImagem;
    }
    public byte[] getPixelFiltrado() {
        return pixelFiltrado;
    }
    
    //Outros Métodos
    public void abrirImagem() throws FileNotFoundException {
        FileInputStream arq = new FileInputStream(getEndereco());
        setArquivo(arq);    
    }
    
    public void extrairDados(){
        try {
            String numeroMagicoLocal = "";
            String larguraLocal = "";
            String comprimentoLocal = "";
            String nivelDeCorLocal = "";
            int contador = 0;
            int caracter;

            while(contador != 3){
                contador++;
                caracter = getArquivo().read();
                //Ignorando Comentários
                if ((char)caracter == '#'){
                    contador --;
                    while((char)caracter != '\n') {
			caracter = getArquivo().read();
                    }
                } else {
                    //Extraindo numero mágico
                    if (contador == 1){
                        while((char)caracter != '\n') {
                            numeroMagicoLocal+=(char)caracter;
                            caracter = getArquivo().read();
                        }
                    //Extraindo largura
                    } else if (contador == 2) {
                        while((char)caracter != ' ') {
                            larguraLocal+=(char)caracter;
                            caracter = getArquivo().read();
                        }
                        caracter = getArquivo().read();
                    //Extraindo comprimento
                        while((char)caracter != '\n') {
                            comprimentoLocal+=(char)caracter;
                            caracter = getArquivo().read();
                        }
                    //Extraindo nivel de cor
                    } else if (contador == 3){
                        while((char)caracter != '\n') {
                            nivelDeCorLocal+=(char)caracter;
                            caracter = getArquivo().read();
                        }
                    }
                }
            }
            
            //Enviando para atributos
            setNumeroMagico(numeroMagicoLocal);
            setComprimento(Integer.parseInt(comprimentoLocal));
            setLargura(Integer.parseInt(larguraLocal));
            setNivelDeCor(Integer.parseInt(nivelDeCorLocal));
            
            //Fechando Imagem
            getArquivo().close();
            
        } catch (IOException ex) {
            Logger.getLogger(Imagem.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex.getMessage() + "\nErro ao extrair dados!", "Ops! Algo não está certo!", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public abstract BufferedImage extrairPixels() throws IOException;
    
    public abstract void calculaTamanhoImagem();
    
    public abstract BufferedImage aplicaFiltroNegativo();
        
    @SuppressWarnings("ConvertToTryWithResources")
    public static int VerificaImagem(String endereco) throws FileNotFoundException, IOException{        
        int contador = 0;
        int caracter;
        //Abrindo Imagem
        FileInputStream arquivo = new FileInputStream(endereco);
        
        //Extraindo Número mágico
        String numeroMagicoLocal = "";
        
        while(contador != 1){
            contador++;
            caracter = arquivo.read();
            //Ignorando Comentários
            if ((char)caracter == '#'){
                contador --;
                while((char)caracter != '\n') {
                    caracter = arquivo.read();
                }
            //Extraindo numero mágico
            } else {
                while((char)caracter != '\n') {
                    numeroMagicoLocal+=(char)caracter;
                    caracter = arquivo.read();
                }
            }
        }
            
        //Fechando Arquivo
        arquivo.close();
            
        //Verificando se a Imagem é correta
        String extensaoNomeImagem = endereco.substring(endereco.length()-3, endereco.length());
        if (numeroMagicoLocal.equals("P5") && extensaoNomeImagem.equals("pgm")){
            return 1; // 1 Se a imagem for realmente PGM
            
        } else if (numeroMagicoLocal.equals("P6") && extensaoNomeImagem.equals("ppm")) {
            return 2; // 2 Se a imagem for realmente PPM
            
        } else {
            return 3; // 3 Se a imagem não tiver o número mágico correspondente a extensaõ do nome do arquivo
            
        }
    }
}