package classes;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.*;

public class PPM extends Imagem{
    //Construtor
    public PPM(String endereco){
        setArquivo(null);
        setPixels(null);
        setNumeroMagico("");
        setEndereco(endereco);
        setLargura(0);
        setComprimento(0);
        setNivelDeCor(0);
        setTamanhoImagem(0);
        setPixelFiltrado(null);
    }

    @Override
    public BufferedImage extrairPixels() throws FileNotFoundException, IOException {
        int contador = 0;
        int contador1 = 0;
        int caracter;
        abrirImagem();

        //Movendo cursor para caracter inicial da imagem
        while (contador != 3) {
            contador++;
            caracter = getArquivo().read();
            if ((char) caracter == '#') {
                contador--;
                while ((char) caracter != '\n') {
                    caracter = getArquivo().read();
                }
            } else {
                while ((char) caracter != '\n') {
                    caracter = getArquivo().read();
                }
            }
        }
        caracter = getArquivo().read();

        //Extraindo os píxels da imagem em forma de Bytes
        BufferedImage imagemPPM = new BufferedImage(getLargura(), getComprimento(), BufferedImage.TYPE_3BYTE_BGR);
        byte[] pixelsLocal = ((DataBufferByte) imagemPPM.getRaster().getDataBuffer()).getData();

        // Pega os Píxels invertidos de R G B para B G R
        while (caracter != -1) {
            if (contador1 == 3 * getComprimento() * getLargura() && (char) caracter == '\n') {

            } else {
                pixelsLocal[contador1 + 2] = (byte) caracter;
                caracter = getArquivo().read();
                contador1++;
                pixelsLocal[contador1] = (byte) caracter;
                caracter = getArquivo().read();
                contador1++;
                pixelsLocal[contador1 - 2] = (byte) caracter;
                contador1++;
            }
            caracter = getArquivo().read();
        }

        //Enviando para atributo
        setPixels(pixelsLocal);

        //Fechando Imagem
        getArquivo().close();

        return imagemPPM;
    }

    @Override
    public void calculaTamanhoImagem() {
    setTamanhoImagem(getLargura()*getComprimento()*3);
    }

    @Override
    public BufferedImage aplicaFiltroNegativo() {
        BufferedImage imagemNegativa = new BufferedImage(getLargura(), getComprimento(), BufferedImage.TYPE_3BYTE_BGR);
        @SuppressWarnings("MismatchedReadAndWriteOfArray")
        byte[] pixelsLocal = ((DataBufferByte) imagemNegativa.getRaster().getDataBuffer()).getData();
        
        for (int i = 0; i < getTamanhoImagem(); i++) {
            pixelsLocal[i]=(byte) (getNivelDeCor() - getPixels()[i]);
        }
        
        setPixelFiltrado(pixelsLocal);
        return imagemNegativa;
    }
    
    public BufferedImage aplicarFiltroRed() {
        BufferedImage imagemRed = new BufferedImage(getLargura(), getComprimento(), BufferedImage.TYPE_3BYTE_BGR);
        @SuppressWarnings("MismatchedReadAndWriteOfArray")
        byte[] pixelsLocal = ((DataBufferByte) imagemRed.getRaster().getDataBuffer()).getData();
        int contadorCiclos = 0;
        
        for (int i = 0; i < getTamanhoImagem(); i++) {
            contadorCiclos++;
            if (contadorCiclos == 1) {
		pixelsLocal[i] = 0;		
            } else if (contadorCiclos == 2) {
		pixelsLocal[i] = 0;
            } else {
		pixelsLocal[i] = getPixels()[i];
		contadorCiclos = 0;
            }
        }
        setPixelFiltrado(pixelsLocal);
        return imagemRed;
    }
    
    public BufferedImage aplicarFiltroGreen() {
        BufferedImage imagemGreen = new BufferedImage(getLargura(), getComprimento(), BufferedImage.TYPE_3BYTE_BGR);
        @SuppressWarnings("MismatchedReadAndWriteOfArray")
        byte[] pixelsLocal = ((DataBufferByte) imagemGreen.getRaster().getDataBuffer()).getData();
        int contadorCiclos = 0;
        
        for (int i = 0; i < getTamanhoImagem(); i++) {
            contadorCiclos++;
            if (contadorCiclos == 1) {
		pixelsLocal[i] = 0;		
            } else if (contadorCiclos == 2) {
		pixelsLocal[i] = getPixels()[i];
            } else {
                pixelsLocal[i] = 0;		
		contadorCiclos = 0;
            }
        }
        setPixelFiltrado(pixelsLocal);
        return imagemGreen;
    }
    
    public BufferedImage aplicarFiltroBlue() {
        BufferedImage imagemBlue = new BufferedImage(getLargura(), getComprimento(), BufferedImage.TYPE_3BYTE_BGR);
        @SuppressWarnings("MismatchedReadAndWriteOfArray")
        byte[] pixelsLocal = ((DataBufferByte) imagemBlue.getRaster().getDataBuffer()).getData();
        int contadorCiclos = 0;
        
        for (int i = 0; i < getTamanhoImagem(); i++) {
            contadorCiclos++;
            if (contadorCiclos == 1) {
		pixelsLocal[i] = getPixels()[i];
            } else if (contadorCiclos == 2) {
		pixelsLocal[i] = 0;		
            } else {
                pixelsLocal[i] = 0;		
		contadorCiclos = 0;
            }
        }
        setPixelFiltrado(pixelsLocal);
        return imagemBlue;
    }
}