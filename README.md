Exercício de Programação 2 - Criado em Java - Utilizando NetBeans

Sobre:
-> O Programa possui interfaces auto-explicativas, e foi criado de forma a facilitar o uso variando entre diversas imagens facilmente, para evitar cliques e passagens de tela desnecessárias;

Funcionamento:
-> O usuário pode abrir imagens do tipo .ppm e .pgm, onde pode aplicar filtros diferentes em cada uma delas e salvá-las como desejar, além de verificar algum tipo de mensagem esteganografada de formas específicas referente ao exercício dado;

Observações "Bugs":
-> Os filtros funcionaram bem pra o exemplo "Lena.pgm", contudo as outras imagens apresentaram uma mudança peculiar, apesar do algoritmo aplicar o filtro corretamente;
-> Alguns caracteres da mensagem gravada nos bits da "Lena.pgm" estão corrompidos, contudo a extração da mensagem está funcionando perfeitamente;
-> Algumas imagens são maiores do que outras, logo é importante ter paciência ao se abrir uma imagem que demore um pouco mais para carregar devido a dimensão que possui;

Tenha um Bom Uso, Obrigado!

